FROM ubuntu:trusty-20170602

# ADD JAVA repo
RUN apt-get update && apt-get install -y curl \
   python-software-properties \
   software-properties-common \
   && add-apt-repository ppa:webupd8team/java

# Install Java
RUN echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
   && echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections \
   && apt-get update && apt-get -y install oracle-java8-installer

# Install Tomcat
RUN mkdir -p /opt/tomcat \
   && curl -SL http://apache.mirrors.nublue.co.uk/tomcat/tomcat-8/v8.5.32/bin/apache-tomcat-8.5.32.tar.gz \
   | tar -xzC /opt/tomcat  --strip-components=1 \
   && rm -Rf /opt/tomcat/webapps/docs /opt/tomcat/webapps/examples


COPY tomcat-users.xml /opt/tomcat/conf/

#Expose tomcat
EXPOSE 8080

ENV JAVA_OPTS -server -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC \
    -Xmx2G

WORKDIR /opt/tomcat/
CMD ["bin/catalina.sh", "run"]


